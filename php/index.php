<!DOCTYPE html>
<html lang="zh-tw">
	<?php include('model/head.php'); ?>
	
	<body>	
		<div class="layout-container">
			<header id="header">
				<div class="col-logo">
					<i class="module-icon icon-logo goto-home"></i>
				</div>
				<div class="col-btn">
					<!-- <span class="btn-header goto-about">ABOUT US</span> -->
					<span class="btn-header goto-portofolio">PORTFOLIO</span>
					<span class="btn-header goto-clients">CLIENTS</span>
					<span class="btn-header goto-contact">CONTACT US</span>
					<!-- <span class="btn-header goto-home">HOME</span> -->
				</div>
				<div class="header-submenu">
				<ul>
					<!-- <li class="btn-submenu goto-about">ABOUT US</li>   -->
					<li class="btn-submenu goto-portofolio">PORTFOLIO</li>
					<li class="btn-submenu goto-clients">CLIENTS</li>
					<li class="btn-submenu goto-contact">CONTACT US</li>
				</ul>
				</div>
				<i class="icon-burger">
					<span></span>
					<span></span>
					<span></span>
				</i>
			</header>
			
			<div id="layout-view" class="layout-view">				
				<section id="section-main" class="section-main">
					<h1>
						WE BUILD THE CONNECTION<br/>
						OF HUMANITIES, CREATION,<br/>
						DESIGN AND IDEA.
					</h1>
					<h2>
						HUMAN,CONSONANCE EMOTION,<br/>
						THINKING,COMMUNICATE,CREATE,<br/>
						INTELLIGENCE AND POWER.<br/>
					</h2>
						<div class="bg-main bg-main1">
						<i class="shape-trangle"></i>
						<i class="shape-plus plus-2"></i>
						<i class="shape-line line-1"></i>
					</div>
					<div class="bg-main bg-main2">
						<i class="shape-plus plus-1"></i>
						<i class="shape-dot"></i>    
					</div>
				</section>
				
				<section class="section-boxbtn">
					<!--   <div class="boxbtn goto-about">
						<p>ABOUT US</p>
						<p>WHO WE ARE.<span>WHO WE ARE.</span></p>
					  </div> -->
					<div class="boxbtn goto-portofolio">
						<p>PORTFOLIO</p>
						<p>WHAT WE DO.<span>WHAT WE DO.</span></p>
					</div>
					<div class="boxbtn goto-contact">
						<p>CONTACT US</p>
						<p>WHERE WE ARE.<span>WHERE WE ARE.</span></p>
					</div>
				</section>

				<?php include('model/about.php'); ?>

				<section class="content-section section-workdetail">
					<div class="workdetail-title">
						<p class="workdetail-title-name"><!-- 作品名稱作品名稱作品名稱作品名稱作品名稱<a class="workdetail-title-url" href=""><i class="module-icon icon-link"></i></a> --></p>
						<p><b>CONCEPT AND OBJECTIVE</b><span>概念與目標</span></p>
					</div>
					<div class="workdetail-msg">
					
					</div>
					<ul class="workdetail-img-list clearfix">
					
					</ul>
					<div class="workdetail-relative">
						<div class="workdetail-title clearfix">
							<p class="goto-portofolio">返回列表</p>
							<p>推薦作品</p>
						</div>
						<ul class="workdetail-relative-list clearfix">
						
						<!-- 
						<li class="workdetail-relative-item item-work" data-num="0">
							<img class="workdetail-relative-item-img" src="http://dummyimage.com/610x320/ddd/fff" width="100%" alt="">
						</li>
						-->
						
						<!-- 
						<li class="workdetail-relative-item item-work" data-num="1">
							<img class="workdetail-relative-item-img" src="http://dummyimage.com/610x320/ddd/fff" width="100%" alt="">
						</li>
						-->
						
						<!-- 
						<li class="workdetail-relative-item item-work" data-num="2">
							<img class="workdetail-relative-item-img" src="http://dummyimage.com/610x320/ddd/fff" width="100%" alt="">
						</li>
						-->
						
						</ul>
					</div>
				</section>

				<?php include('model/portofolio.php'); ?>

				<?php include('model/clients.php'); ?>

				<?php include('model/contact.php'); ?>
			</div>
			
			<?php include('model/footer.php'); ?>		
			
			<div class="layout-lightbox">
				<div class="layout-lightbox-bg"></div>
				<div class="layout-lightbox-content">
					<h3>謝謝您的來信！<br/>我們會盡快與您連繫！</h3>
					<button class="form-btn do-closepopup">返回首頁</button>
				</div>
			</div>
		</div>
		<div class="layout-loading">
			<div class="layout-loading-top">
				<div class="loading-top-logo">
					<img src="images/img-logo-drama-w300.png" width="100%" alt="">
					<h3>多瑪數位創意時空></h3>
          <span class="form-btn">
            ENTER
            <span class="form-btn-overlay">
              <i class="form-btn-overlay-dot1"></i>
              <i class="form-btn-overlay-dot2"></i>
              <i class="form-btn-overlay-dot3"></i>
              <i class="form-btn-overlay-dot4"></i>
            </span>
          </span>
				</div>
				<div class="layout-loading-top-bg loading-top-bg1">
					<i class="shape-trangle"></i>
					<i class="shape-plus plus-2"></i>
				</div>
				<div class="layout-loading-top-bg loading-top-bg2">
					<i class="shape-plus plus-1"></i>
					<i class="shape-line line-1"></i>
					<i class="shape-line line-2"></i>
					<i class="shape-line line-3"></i>
					<i class="shape-dot"></i>    
				</div>
			</div>
			<div class="layout-loading-bottom"></div>
		</div>
	</body>
</html>