'use strict';

$(function() {
	  //close loading cut
	function removeLoading(delaytime){
		$("body").delay(delaytime).queue(
			function(next){$(this).addClass('is-fading');
		next();}).delay(2000).queue(
			function(next){$(this).addClass("is-loaded").find(".layout-loading").remove();
		next();});
	}
	
    $("body").on("click",".layout-loading .form-btn.is-loaded",function(){
        removeLoading(0);
    });

    $(".list-work,.workdetail-relative-list").hover(
    function() {
    $( this ).addClass("is-focusing");
    }, function() {
    $( this ).removeClass("is-focusing");
    }
    );


	$(window).load(function() {
		// alert("page loaded");
        $(".layout-loading .form-btn").addClass('is-loaded');
		// removeLoading(7000);
	});

	//mousemove event to get position
	var x = null;
	var y = null;

	document.addEventListener('mousemove', onMouseUpdate, false);
	document.addEventListener('mouseenter', onMouseUpdate, false);

	function onMouseUpdate(e) {
		x = e.pageX;
		y = e.pageY;
		var w = window.innerWidth;
		var h = window.innerHeight;      
		// console.log( "x="+x+",y="+y+"w="+w+",h="+h);
		//console.log( "x=" + (x-(0.5*w))/w + ",y="+ (y-(h*0.5))/h );
		var myvalue = " translate("+(x-(0.5*w))/w*20*(-1)+"px, "+(y-(h*0.5))/h*12*(-1)+"px)";
		var myvalue2 = " translate("+(x-(0.5*w))/w*6+"px, "+(y-(h*0.5))/h*4+"px)";
		var myvalue3 = " translate("+(x-(0.5*w))/w*60*(-1)+"px, "+(y-(h*0.5))/h*40*(-1)+"px)";
		// $(".layout-loading-top-bg.loading-top-bg1,.bg-main.bg-main1xxx").css("transform",myvalue3).css("-webkit-transform",myvalue3).css("-moz-transform",myvalue3).css("-o-transform",myvalue3).css("-ms-transform",myvalue3);
		// $(".layout-loading-top-bg.loading-top-bg2,.bg-main.bg-main2xxx").css("transform",myvalue).css("-webkit-transform",myvalue).css("-moz-transform",myvalue).css("-o-transform",myvalue).css("-ms-transform",myvalue);
		// $(".loading-top-logo").css("transform",myvalue2).css("-webkit-transform",myvalue2).css("-moz-transform",myvalue2).css("-o-transform",myvalue2).css("-ms-transform",myvalue2);
		// $(".layout-loading-top-bg.loading-top-bg1").css( { "transform" : myvalue3 , "webkitTransform" : myvalue3 , "MozTransform" : myvalue3 , "OTransform" : myvalue3 , "msTransform" : myvalue3 });
	}

	function getMouseX() {
		return x;
	}

	function getMouseY() {
		return y;
	}
	
    //scroll events to slide header
    var sheight   = window.innerHeight;
    var swidth    = window.innerWidth;
    var scrollNow = 0;
    $(window).scroll(function () {
        var scrollVal = $(this).scrollTop();
        
        if(scrollVal>scrollNow && scrollNow>100){
            $("#header").addClass("is-slideup");
        }else
        if(scrollVal<scrollNow ){
            $("#header").removeClass("is-slideup");
        }
        scrollNow     = scrollVal;  
    });

  //get works and clients from json
    var works   = [];
    var _works  = []; //add by danny 20160120(用來刪除看過的作品，置放入you may also like)
    var clients = [];
    function getWorks(){
        /*$.getJSON('javascripts/works.json', function(json, textStatus) {
            $.each(json, function(index,mydata) {
                works[index] = {};
                works[index] = json[index];
                $(".list-work").append('<li class="item-work" data-num='+ index +'><div class="item-work-imgbox"><img class="item-work-img" src=' + works[index].image +' width="100%" alt=""></div><div class="item-work-describe"><p class="item-work-name">'+ json[index].title +'</p><!--<p class="item-work-msg">'+ json[index].describe +'</p>--></div></li>');
            });
        });*/

        $.ajax({
            url:'api/api.php'
            ,data:{
                api : 'work.list'
            }
            ,type:'POST'
            ,dataType:'JSON'
            //,async:false
            //,beforeSend:
            ,error:function(textStatus){
                alert('Work data error!');
            }
            ,success:function(json){
                $.each(json, function(key,val) {
                    works[key] = {};
                    works[key] = json[key];
					
					var li 	= 	'<li class="item-work" data-num='+ key +' onclick="ga(\'send\', \'event\', \'作品列表\', \'click\', \''+ works[key].work_name +'\');">';
					li 		+=		'<div class="item-work-img">';
					li  	+=			'<div class="item-work-img-content" style="background-image: url('+ works[key].work_image +');"></div>';
					li 	    +=		'</div>';
					li 		+=		'<div class="item-work-describe"><p class="item-work-name">'+ json[key].work_name +'</p></div>';
					li 		+=	'</li>';
					
                    //$(".list-work").append('<li class="item-work" data-num='+ key +'><div class="item-work-imgbox"><img class="item-work-img" src=' + works[key].work_image +' width="100%" height="312px" alt=""></div><div class="item-work-describe"><p class="item-work-name">'+ json[key].work_name +'</p><!--<p class="item-work-msg">'+ json[key].work_concept +'</p>--></div></li>');
                    $(".list-work").append(li);
                });
                _works = $.extend(true,[],works);
            }
        });
    };
    function getClients(){
        /*$.getJSON('javascripts/clients.json', function(json, textStatus) {
            $.each(json, function(index,mydata) {
                clients[index] = {};
                clients[index] = json[index];
                $(".list-client").append('<li class="item-client" data-num='+ index +' ><img class="item-client-img" src='+ clients[index].logo +' alt=""><div class="item-client-describe"><p class="item-client-name">'+ clients[index].name +'</p><p class="item-client-msg">'+ clients[index].describe +'</p></div></li>');
            });
        });*/

        $.ajax({
            url:'api/api.php'
            ,data:{
                api : 'client.list'
            }
            ,type:'POST'
            ,dataType:'JSON'
            //,async:false
            //,beforeSend:
            ,error:function(textStatus){
                alert('Client data error!');
				//console.log(textStatus);
            }
            ,success:function(json){
                $.each(json, function(key,val) {
                    clients[key] = {};
                    clients[key] = json[key];
                    $(".list-client").append('<li class="item-client" data-num='+ key +' ><img class="item-client-img" src='+ clients[key].client_image +' alt=""><div class="item-client-describe"><p class="item-client-name">'+ clients[key].client_name +'</p><p class="item-client-msg">'+ clients[key].client_content +'</p></div></li>');
                });
            }
        });
    };
    //click event to toggle lightboz
    /*$("body").on("click",".do-openpopup",function(){
        $(".layout-lightbox").addClass("is-open").delay(100).queue(
            function(next){$(this).addClass("is-showing");
        next();});
    });*/
    $("body").on("click",".do-closepopup",function(){
        $(".layout-lightbox").removeClass("is-showing").delay(900).queue(
            function(next){$(this).removeClass("is-open");
        next();});
    });
    //click events to switch menu
    $("#header").on("click",".icon-burger,.header-submenu",function(){
        $("#header").toggleClass("is-open");
    });
    $("body").on("click",".goto-about",function(){
        $(".section-workdetail").removeClass("is-showing");
        $("body").removeClass().addClass("is-open-about");
        var nextscroll = $('.section-boxbtn').offset().top;
        $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
        window.location = "#about";
    });
    $("body").on("click",".goto-portofolio",function(){
        $(".section-workdetail").removeClass("is-showing");
        $("body").removeClass().addClass("is-open-portofolio");
        var nextscroll = $('.section-boxbtn').offset().top;
        $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
        window.location = "#portofolio";
    });
    $("body").on("click",".goto-clients",function(){
        $(".section-workdetail").removeClass("is-showing");
        $("body").removeClass().addClass("is-open-clients");
        var nextscroll = $('.section-boxbtn').offset().top;
        $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
        window.location = "#clients";
    });
    $("body").on("click",".goto-contact",function(){
        $(".section-workdetail").removeClass("is-showing");
        $("body").removeClass().addClass("is-open-contact");
        var nextscroll = $('.section-boxbtn').offset().top;
        $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
        window.location = "#contact";
    });
    $("body").on("click",".goto-home",function(){
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-loaded");
      $("#header").removeClass("is-open");
      window.location = "#";
        //window.location = "/";
        // $('html,body').animate({ scrollTop:0 }, 800);
    });

    //click events to switch focus style
    $("body").on("focus",".module-form-input,.module-form-textarea",function(){
        $("body").find(".is-focus").removeClass('is-focus');
        $(this).closest(".form-block").addClass("is-focus");
    });
    $("body").on("blur",".module-form-input,.module-form-textarea",function(){
        $(this).closest(".form-block").removeClass("is-focus");
    });

    function openDetail(myindex){
        /*清空作品LIST & You may also like List，再擺入新的START*/        
        $(".workdetail-img-list").empty();
        $(".workdetail-relative-list").empty();
        /*清空作品LIST & You may also like List，再擺入新的END*/

        /*避免You may also like出現看過的作品，全部看完剩三則時，再重新迴圈START*/
        if(_works.length <= 3){
            _works = $.extend(true,[],works);
        }else{
            var looked_key;
            $.each(_works,function(key,val){
                if(works[myindex].work_id == val.work_id){
                    looked_key = key;
                }
            });

            if(typeof(looked_key) != 'undefined'){
                _works.splice(looked_key,1);
            }
        }
        /*避免You may also like出現看過的作品，全部看完剩三則時，再重新迴圈END*/   

        $(".section-workdetail").removeClass("is-showing").queue(
            function(next){
                
            var that   = $(this);       

            $(this).find(".workdetail-title-name").html( works[myindex].work_name + '<a class="workdetail-title-url" href='+ works[myindex].work_url +' target="_blank" ><i class="module-icon icon-link"></i></a>');
            $(this).find(".workdetail-msg").text(works[myindex].work_concept);
            //detail img may be 1~4 pieces
            if(works[myindex].work_photo == ''){
                var li  = '<li class="workdetail-img-item">';
                    li      +='<img class="item-workdetail-img" src='+ works[myindex].work_image +' width="100%" alt="">';
                    li      +='</li>';
                    
                that.find(".workdetail-img-list").append(li);
            }else{
                var count = 0;	
				var li  = '<li class="workdetail-img-item">';
                    li  +='<img class="item-workdetail-img" src='+ works[myindex].work_image +' width="100%" alt="">';
                    li  +='</li>';
				
                $.each(works[myindex].work_photo,function(key,val){
                    if(count < 4){
                        li  	+= '<li class="workdetail-img-item">';
                        li      +='<img class="item-workdetail-img" src='+ val +' width="100%" alt="">';
                        li      +='</li>';  
                        
                        count++;
                    }
                });
				that.find(".workdetail-img-list").append(li);
            }

            //relative works must be 3 pieces
            _works.sort(function(){return Math.random()>0.5?-1:1;});
            for(var i = 0 ; i < 3 ; i++){
                var main_key;
                $.each(works,function(key,val){
                    if(val.work_id == _works[i].work_id){
                        main_key = key;
                    }
                });
                var li  = '<li class="workdetail-relative-item item-work" data-num='+ main_key +' onclick="ga(\'send\', \'event\', \'關係清單\', \'click\', \''+ _works[i].work_name +'\');">';
                li      += 	'<div class="workdetail-relative-item-img">';
				li   	+= 		'<div class="workdetail-relative-item-img-content" style="background-image: url('+ _works[i].work_image +');"></div></div></li>';
				
                that.find(".workdetail-relative-list").append(li);
            }
            //$(this).find(".workdetail-relative-list").html('<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate1 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate1].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate2 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate2].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate3 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate3].image+' width="100%" alt=""></div></li>');
        next();}).delay(300).queue(
            function(next){$(this).addClass("is-showing");
        next();}); 
    };

    function switchDetail(myindex){
        $(".workdetail-img-list").empty();
        $(".workdetail-relative-list").empty();

        if(_works.length <= 3){
            _works = $.extend(true,[],works);
        }else{
            var looked_key;
            $.each(_works,function(key,val){
                if(works[myindex].work_id == val.work_id){
                    looked_key = key;
                }
            });

            if(typeof(looked_key) != 'undefined'){
                _works.splice(looked_key,1);
            }
        }

        $(".section-workdetail").addClass("is-fading").delay(350).queue(
            function(next){
            var that    = $(this);
                
            $(this).find(".workdetail-title-name").html(works[myindex].work_name + '<a class="workdetail-title-url" href='+ works[myindex].work_url +' target="_blank" ><i class="module-icon icon-link"></i></a>');
            $(this).find(".workdetail-msg").text(works[myindex].work_concept);
            //detail img may be 1~4 pieces
            if(works[myindex].work_photo == ''){
                var li  = '<li class="workdetail-img-item">';
                    li      +='<img class="item-workdetail-img" src='+ works[myindex].work_image +' width="100%" alt="">';
                    li      +='</li>';
                    
                that.find(".workdetail-img-list").append(li);
            }else{
                var count = 0;  
				var li  = '<li class="workdetail-img-item">';
                    li  +='<img class="item-workdetail-img" src='+ works[myindex].work_image +' width="100%" alt="">';
                    li  +='</li>';
				
                $.each(works[myindex].work_photo,function(key,val){
                    if(count < 4){
                        li 	 	+= '<li class="workdetail-img-item">';
                        li      +='<img class="item-workdetail-img" src='+ val +' width="100%" alt="">';
                        li      +='</li>';
                        
                        count++;
                    }
                });
				that.find(".workdetail-img-list").append(li);
            }

            //relative works must be 3 pieces
            _works.sort(function(){return Math.random()>0.5?-1:1;});
            for(var i = 0 ; i < 3 ; i++){
                var main_key;
                $.each(works,function(key,val){
                    if(val.work_id == _works[i].work_id){
                        main_key = key;
                    }
                });
                var li  = '<li class="workdetail-relative-item item-work" data-num='+ main_key +' onclick="ga(\'send\', \'event\', \'關係清單\', \'click\', \''+ _works[i].work_name +'\');">';
                li      += 	'<div class="workdetail-relative-item-img">';
				li   	+= 		'<div class="workdetail-relative-item-img-content" style="background-image: url('+ _works[i].work_image +');"></div></div></li>';

                that.find(".workdetail-relative-list").append(li);
            }
            //$(this).find(".workdetail-relative-list").html('<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate1 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate1].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate2 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate2].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate3 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate3].image+' width="100%" alt=""></div></li>');
        next();}).delay(350).queue(
            function(next){$(this).removeClass("is-fading");
        next();});     
    };

    //click events to switch works detail
    $("body").on("click",".item-work",function(){
        var index = $(this).attr("data-num");
        window.location = "#work="+index ;
        if ( !$("body").find(".section-workdetail").hasClass("is-showing") ){
            openDetail(index);
        }else{
            switchDetail(index);
        }
        var nextscroll = $('.section-workdetail').offset().top;
        $('html,body').animate({ scrollTop:( parseInt(nextscroll-100) )}, 600);
    });
      
    $(".section-boxbtn").on("click",".boxbtn",function (){
        $(this).closest('.section-boxbtn').toggleClass('is-active').find(".boxbtn").not(this).removeClass('is-on');
        $(this).toggleClass('is-on');
    });

    //get hash
  function getHash(){
    var myhash = location.hash.slice(1);
    var hasharray = myhash.split("=");
    // alert("hash="+hasharray[0]+" , hasharray[1]="+hasharray[1]);
    if ( hasharray[0] == "work" ){
      $(".layout-loading .form-btn").remove();
      removeLoading(1000);
      //goto portofolio
      var hashindex = parseInt(hasharray[1]);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-portofolio").delay(1000).queue(
          function(next){
            openDetail(hashindex);
            next();});
    } else
    if( hasharray[0] == "about" ){
      $(".layout-loading .form-btn").remove();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-about");
    } else
    if( hasharray[0] == "portofolio" ){
      $(".layout-loading .form-btn").remove();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-portofolio");
    } else
    if( hasharray[0] == "clients" ){
      $(".layout-loading .form-btn").remove();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-clients");
    } else
    if( hasharray[0] == "contact" ){
      $(".layout-loading .form-btn").remove();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-contact");
    }
  }
    //auto add height
	$('textarea').on("focus",function(){
		var enterpressed = 0;
		$(this).on("keypress",function(e){
			var code = e.keyCode || e.which;
			if(code == 13) { //keycode of enter
				enterpressed ++;
				if(enterpressed > 2){
					var myheight = parseInt($(this).height());
					if ( myheight < 200 ){
					$(this).height( myheight+24);
					}
				}
			}
		});
	});
	
	//animation
	function AnimateLine1(){
		//for slogan line1
		$("body").find(".bg-main.bg-main1").find(".shape-line.line-1").remove();
		$("body").find(".bg-main.bg-main1").append('<i class="shape-line line-1"></i>')
			.find(".shape-line.line-1").css("margin-left", Math.random()*(-400) ).addClass("do-animate")
			.delay(7000).queue(function(next){	
				AnimateLine1();
		    next();});	
	};
	function AnimateLine2(){
		//for slogan line2
		$("body").find(".bg-main.bg-main1").find(".shape-line.line-2").remove();
		$("body").find(".bg-main.bg-main1").append('<i class="shape-line line-2"></i>')
			.find(".shape-line.line-2").css("margin-left", Math.random()*(-300) ).addClass("do-animate")
			.delay(13000).queue(function(next){
				AnimateLine2();
			next();});
	};
	function AnimateLine3(){
		$("body").find(".layout-loading-top-bg.loading-top-bg1").find(".shape-line.line-1").remove();
		$("body").find(".layout-loading-top-bg.loading-top-bg1").append('<i class="shape-line line-1"></i>')
			.find(".shape-line.line-1").css("margin-left", (Math.random()-0.5)*200 ).addClass("do-animate")
			.delay(10000+(Math.random()-0.5)*1000).queue(function(next){
				AnimateLine3();
			next();});
	};
	function AnimateLine4(){
		$("body").find(".layout-loading-top-bg.loading-top-bg1").find(".shape-line.line-2").remove();
		$("body").find(".layout-loading-top-bg.loading-top-bg1").append('<i class="shape-line line-2"></i>')
			.find(".shape-line.line-2").css("margin-left", (Math.random()-0.5)*300 ).addClass("do-animate")
			.delay(13000+(Math.random()-0.5)*1000).queue(function(next){
				AnimateLine4();
			next();});
	};
	function AnimateLine5(){
		$("body").find(".layout-loading-top-bg.loading-top-bg1").find(".shape-line.line-3").remove();
		$("body").find(".layout-loading-top-bg.loading-top-bg1").append('<i class="shape-line line-3"></i>')
			.find(".shape-line.line-3").css("margin-left", (Math.random()-0.5)*250 ).addClass("do-animate")
			.delay(7000+(Math.random()-0.5)*1000).queue(function(next){
				AnimateLine5();
		    next();});
	};

	function doSloganAnimate(){
		AnimateLine1();
		AnimateLine2();
	}
	function doLoadingAnimate(){
		AnimateLine3();
		AnimateLine4();
		AnimateLine5();
	}

    // 執行 FastClick
    FastClick.attach(document.body);
    getWorks();
    getClients();
    getHash();
});