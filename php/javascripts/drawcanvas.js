$(function () {
var fullWidth = parseInt($("#layout-view").width());
var fullHeight = parseInt($("#section-main").height());
var canvas = document.createElement("canvas");
canvas.width = fullWidth;
canvas.height = fullHeight;
document.getElementById("section-main").appendChild(canvas);
$("#section-main").find("canvas").addClass('bg-main'); 
var context = canvas.getContext("2d");
var speed = 0.2;
var t = 50;/* Start from which frame */

function RandOne(){
  var r = Math.random();
    if(r>0.5){
      return 1
    }else{
      return -1
    }
};

function addCircle(newX,newY) {
  var circle = new Path2D();
  circle.moveTo(400,200);
  circle.arc(newX,newY, 3, 0, 2 * Math.PI, true);
  context.fillStyle="#727272";
  context.fill(circle);
};

function addTriangle(newX,newY,newAngle) {
  var newCos = Math.cos(newAngle/Math.PI/2);
  var newSin = Math.sin(newAngle/Math.PI/2);
  context.beginPath();
  context.moveTo(newX+(20/3) - newSin*10 , newY - newCos*10);
  context.lineTo(newX-(10/3) - newSin*10 , newY+(10/Math.sqrt(3)) - newCos*10);
  context.lineTo(newX-(10/3) - newSin*10 , newY-(10/Math.sqrt(3)) - newCos*10);
  context.fillStyle="#727272";
  context.fill();
};

function addCross(newX,newY,newAngle,r) {
    var newCos = Math.cos(newAngle/Math.PI/2);
    var newSin = Math.sin(newAngle/Math.PI/2);
    context.beginPath();
    context.moveTo(newX + newCos*r ,newY+ newSin*r);
    context.lineTo(newX -  newCos*r,newY - newSin*r);
    context.fillStyle="#727272";
    context.stroke();
    context.beginPath();
    context.moveTo(newX - newSin*r ,newY+ newCos*r);
    context.lineTo(newX +  newSin*r,newY - newCos*r);
    context.fillStyle="#727272";
    context.stroke();
};

function addLine(newX,newY,t,r) {
    var newAngle = -3;
    var newCos = Math.cos(newAngle/Math.PI/2);
    var newSin = Math.sin(newAngle/Math.PI/2);
    context.beginPath();
    // context.moveTo(newX + newCos*r-t ,newY+ newSin*r+t);
    context.moveTo(newX-t ,newY+t);
    context.lineTo(newX - newCos*r*(t/1)-t,newY - newSin*r*(t/1)+t);
    context.strokeStyle="#727272";
    context.stroke();
};

function drawit() {
  //Fill canvas itself
  // if( $("html").find("body").hasClass('is-open-portofolio') ){
  //   context.fillStyle="rgba(0,0,0,0)";
  // }else{
  //   context.fillStyle="#232323";    
  // }
  // context.globalAlpha = "0.1";
  context.fillStyle="#ffffff";
  context.fillRect(0, 0, canvas.width, canvas.height);
  //Draw a Circle
  // addCircle( 1500-t%(fullHeight*2.5)-Math.cos(t/Math.PI/2)*8 , -3+(t%(fullHeight*2.5)/2) );
  addCircle( 1350 , 120 );
  // addTriangle( 20-t+RandOne()*0.2 , 100+(t/1.3) , t );
  addTriangle(30,140,1);
  // addCross(1550,200,t*(-0.2),10);
  addCross(1550,200,45,10);
  // addCross(720,40,t*0.1,6);
  addCross(720,40,40,6);
  addLine(1230,320,1,60);
  // addLine(1700,0,t,60);
  t += speed;
  requestAnimationFrame(drawit);
};

drawit();

});
