<footer id="footer">
	<div class="footer-content">
		<span>
			<b>多瑪數位廣告股份有限公司</b>
		</span>
		<span class="style-noborder">
			COPYRIGHT © DRAMA NETWORK. INC ALL RIGHTS RESERVED
		</span>
		<span>
			TEL : 02 2595 9595
		</span>
		<span>
			FAX : 02 2595 9530
		</span>
		<a href="mailto:service@drama.com.tw">
			<i class="fa fa-envelope-o"></i>  
		</a>
	</div>
</footer>