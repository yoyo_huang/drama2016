<script language="javascript">
	function checkinput(){
		var flag = true;
		/* $(".require").each(function(index,element){
			switch($(element).attr('name')){
				case 'contact_tel':
					if( $(this).val() == '' ){
						alert('請填寫完整資料');
						$(this).focus();
						flag = false;
						return false;
					}else if( !is_tel($(this).val()) && !is_mobile($(this).val()) ){
						alert('您輸入的電話格式有誤');
						$(this).focus();
						flag = false;
						return false;
					}
					break;
				case 'contact_mail':
					if( $(this).val() == '' ){
						alert('請填寫完整資料');
						$(this).focus();
						flag = false;
						return false;
					}else if( !is_mail_format($(this).val()) ){
						alert('您輸入的信箱格式有誤');
						$(this).focus();
						flag = false;
						return false;
					}
					break;
				default:
					if( $(this).val() == '' ){
						alert('請填寫完整資料');
						$(this).focus();
						flag = false;
						return false;
					}
					break;
			}
		}); */
		
		$(".require").each(function(index,element){
			if( $(this).val() == '' ){
				alert('請填寫完整資料');
				$(this).focus();
				flag = false;
				return false;
			}
		});
		
		if(flag){
			//$("form").first().submit();
			ajax_submit();
		}
	}

	function ajax_submit(){
		$.ajax({
			url:'/controller/contact_control.php'
			,data:{
				contact_name 	: $("input[name='contact_name']").val()
				,contact_tel 	: $("input[name='contact_tel']").val()
				,contact_mail 	: $("input[name='contact_mail']").val()
				,contact_msg 	: $("textarea[name='contact_msg']").val()
			}
			,type:'POST'
			//,dataType:
			//,async:
			,error:function(textStatus){
				alert('送出失敗');
			}
			,success:function(data){
				if(data == 'success'){
					//alert('感謝您的意見回饋，我們將盡快回覆您');
					$(".require").val('');
					$(".layout-lightbox").addClass("is-open").delay(100).queue(
			            function(next){$(this).addClass("is-showing");
			        next();});
				}else{
					alert('繳交失敗');
				}
			}
		});
	}

	$(function(){

	});
</script>

<section class="content-section section-contact">
	<div class="content-section-title">CONTACT US</div>
	<!--   
	<div class="boxbtn">
	<p>CONTACT US</p>
	<p>WHERE WE ARE.</p>
	</div>
	-->
	<div class="section-contact-profile clearfix"> 
		<div class="col-left">
			<!-- <p class="content-article"><i class="module-icon icon-logo"></i></p> -->
      <p class="content-article style-xl">
        <i class="module-icon icon-phone"></i>(886)2 2595 9595
      </p>
      <p class="content-article style-xl">
        <i class="module-icon icon-fax"></i>(886)2 2595 9530
      </p>
      <p class="content-article style-xl">
        <i class="module-icon icon-mail"></i>service@drama.com.tw
      </p>
      <p class="content-article  pt1">
        多瑪數位廣告股份有限公司
      </p>
			<p class="content-article pb0">
				DRAMA DIGITAL ADVERTISING INC.
			</p>
			<p class="content-article pb0">
				103台北市大同區重慶北路三段252巷16號1樓
			</p>
			<p class="content-article pb0">
				1F., No.16, Ln. 252, Sec. 3, Chongqing N. Rd., Datong Dist., Taipei City 103, Taiwan (R.O.C.)
			</p>
		</div>
		<div class="col-right">
			<p class="content-article style-large">
				若有任何問題與指教，請與我們連繫。
			</p>
			<form id="contactForm" class="module-form">
				<div class="form-block style-half">
				<label class="module-form-label" for="contact_name">連絡姓名</label>
				<input type="text" class="module-form-input input-name require" name="contact_name" placeholder="" maxlength="20" />
				<!-- <input type="text" name="email" class="module-form-input" placeholder="請輸入e-mail" /> -->
				<!-- <p class="form-warning-text"></p> -->
				</div>
				<div class="form-block style-half">
				<label class="module-form-label" for="contact_tel">連絡電話</label>
				<input type="text" class="module-form-input input-phone style-half require" name="contact_tel" placeholder="" maxlength="15" />
				<!-- <input type="text" name="email" class="module-form-input" placeholder="請輸入e-mail" /> -->
				<!-- <p class="form-warning-text"></p> -->
				</div>
				<div class="form-block style-full">
				<label class="module-form-label" for="contact_mail">連絡信箱</label>
				<input type="text" class="module-form-input input-mail require" name="contact_mail" placeholder="" maxlength="50" />
				<!-- <input type="text" name="email" class="module-form-input" placeholder="請輸入e-mail" /> -->
				<!-- <p class="form-warning-text"></p> -->
				</div>
				<div class="form-block style-full style-textarea">
				<label class="module-form-label" for="contact_msg">意見回饋</label>
				<textarea class="module-form-textarea input-msg require" name="contact_msg" placeholder=""></textarea>
				<!-- <input type="text" name="email" class="module-form-input" placeholder="請輸入e-mail" /> -->
				<!-- <p class="form-warning-text"></p> -->
				</div>
			</form>
			<button id="submit" class="form-btn btn-send do-openpopup" onclick="checkinput();">發送</button>
		</div>
	</div>
	<div id="map-canvas" class="section-contact-map">
	
	</div>
</section>