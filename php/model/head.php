<head> 
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0" />
	<meta name="Title" content="Drama Network" />
	<meta name="Author" content="Yoyo Huang@Drama Network" />
	<meta name="Description" content="多瑪數位創意時空" />
	<!-- iOS -->
	<!-- Android -->
	<meta property="og:title" content="Drama Network" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://www.drama.com.tw/" />
	<meta property="og:image" content="images/share.jpg" />
	<meta property="og:site_name" content="Drama Network" />
	<meta property="og:description" content="多瑪數位創意時空" />
	<meta property="og:locality" content="Taipei" />
	<meta property="og:country-name" content="Taiwan" />
	<!--link href="images/favicon.ico" rel="icon" type="image/x-icon" /-->
	<link href="favicon.png" rel="shortcut icon" type="image/png">
	<title>Drama Network 多瑪數位創意時空</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,300,300italic,400italic,600,600italic,700,700italic,800italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lato:400,300,100,100italic,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href="stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /><link href="stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />  <link href="stylesheets/units/main.css" media="screen" rel="stylesheet" type="text/css" />
	
	<script src="javascripts/vendors/jquery.min.js" type="text/javascript"></script>
	<script src="javascripts/vendors/fastclick.js" type="text/javascript"></script>
	<script src="javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script>
	<script src="javascripts/vendors/jquery.lazyload.min.js" type="text/javascript"></script>
	<script src="javascripts/initialization.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="javascripts/map.js" type="text/javascript"></script>
	<script src="api/js/func.js" type="text/javascript"></script>
	<script language="javascript">
		$(function(){
			$("img").lazyload({
				effect:'fadeIn'
			});
		});
	</script>
	<script>
	<!--GA Start-->
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-72604416-1', 'auto');
		ga('send', 'pageview');
	<!--GA END-->
	</script>
</head>