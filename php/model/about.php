<section class="content-section section-about">
	<div class="content-section-title">ABOUT US</div>
	<!--
	<div class="boxbtn">
	<p>ABOUT US</p>
	<p>WHO WE ARE.</p>
	</div>
	-->
	<p class="content-article"><i class="module-icon icon-logo"></i></p>
	<p class="content-article">
	我們認為DRAMA對於品牌是最根本，也是最重要的一種「行為」，<br/>
	任何能產生效益的行為，都需要一種能在消費者心中發生共鳴的元素，<br/>
	這個元素，就是我們所認為DRAMA的重點核心，<br/>
	我們已經脫離傳統廣告對於DRAMA的暨有概念，並且把難以捉摸的創意概念公式化，<br/>
	我們把DRAMA提升為一種「動詞」，進而以DRAMA為企業精神。
	</p>
	<p class="content-article">
	對多瑪來說，<br/>
	知識用來解決問題，<br/>
	創意用來尋找問題，<br/>
	並且我們認為尋找問題與解決問題同等重要！
	</p>
	<p class="content-article">
	在每一次不同的專案中，<br/>
	有幸接觸到各行各業，<br/>
	讓我們涉入各種不同的專業領域，<br/>
	經過一次又一次的體會與瞭解，<br/>
	使多瑪成為一座載有人文、知識，並擁有組織、洞察力的滙集平台。
	</p>
	<p class="content-article">
	不管在業界已過了多少年， 多瑪的每一位成員，<br/>
	對設計、創意、行銷的熱情，依然有增無減。<br/>
	不斷在自己與客戶的領域中充實，<br/>
	精進的心‧有目共睹！<br/>
	</p>
</section>