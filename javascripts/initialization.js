'use strict';

$(".layout-loading").load(function() {
  // alert("page loaded");
  // removeLoading(5000);
});

(function() {
  //ENTEr BUTTON SHOW ON DOCUMENT READY
  $(".layout-loading .form-btn").addClass('is-loaded');

  //close loading cut
  function removeLoading(delaytime){
    $("body").delay(delaytime).queue(
          function(next){$(this).addClass('is-fading');
        next();}).delay(2000).queue(
          function(next){$(this).addClass("is-loaded").find(".layout-loading").remove();
        next();});
  }

  $("body").on("click",".layout-loading .form-btn.is-loaded",function(){
    removeLoading(0);
  });

  $(".list-work,.workdetail-relative-list").hover(
  function() {
    $( this ).addClass("is-focusing");
  }, function() {
    $( this ).removeClass("is-focusing");
  }
);

  //mousemove event to get position
  var x = null;
  var y = null;

  document.addEventListener('mousemove', onMouseUpdate, false);
  document.addEventListener('mouseenter', onMouseUpdate, false);

  function onMouseUpdate(e) {
      x = e.pageX;
      y = e.pageY;
      var w = window.innerWidth;
      var h = window.innerHeight;      
      // console.log( "x="+x+",y="+y+"w="+w+",h="+h);
      console.log( "x=" + (x-(0.5*w))/w + ",y="+ (y-(h*0.5))/h );
      var myvalue = " translate("+(x-(0.5*w))/w*20*(-1)+"px, "+(y-(h*0.5))/h*12*(-1)+"px)";
      var myvalue2 = " translate("+(x-(0.5*w))/w*6+"px, "+(y-(h*0.5))/h*4+"px)";
      var myvalue3 = " translate("+(x-(0.5*w))/w*60*(-1)+"px, "+(y-(h*0.5))/h*40*(-1)+"px)";
      // $(".layout-loading-top-bg.loading-top-bg1,.bg-main.bg-main1xxx").css("transform",myvalue3).css("-webkit-transform",myvalue3).css("-moz-transform",myvalue3).css("-o-transform",myvalue3).css("-ms-transform",myvalue3);
      // $(".layout-loading-top-bg.loading-top-bg2,.bg-main.bg-main2xxx").css("transform",myvalue).css("-webkit-transform",myvalue).css("-moz-transform",myvalue).css("-o-transform",myvalue).css("-ms-transform",myvalue);
      // $(".loading-top-logo").css("transform",myvalue2).css("-webkit-transform",myvalue2).css("-moz-transform",myvalue2).css("-o-transform",myvalue2).css("-ms-transform",myvalue2);
      // $(".layout-loading-top-bg.loading-top-bg1").css( { "transform" : myvalue3 , "webkitTransform" : myvalue3 , "MozTransform" : myvalue3 , "OTransform" : myvalue3 , "msTransform" : myvalue3 });
  }

  function getMouseX() {
      return x;
  }

  function getMouseY() {
      return y;
  }

  //scroll events to slide header
  var sheight = window.innerHeight;
  var swidth = window.innerWidth;
  var scrollNow = 0;
  $(window).scroll(function () {
    var scrollVal = $(this).scrollTop();

    if(scrollVal>scrollNow && scrollNow>100){
        $("#header").addClass("is-slideup");
    }else
    if(scrollVal<scrollNow ){
        $("#header").removeClass("is-slideup");
    }
    scrollNow = scrollVal;  
  });

      
  //get works and clients from json
  var works = [];
  var clients = [];
  function getWorks(){
    $.getJSON('/javascripts/works.json', function(json, textStatus) {
      $.each(json, function(index,mydata) {
        works[index] = {};
        works[index] = json[index];
        // $(".list-work").append('<li class="item-work" data-num='+ index +'><div class="item-work-imgbox"><img class="item-work-img" src=' + works[index].image +' width="100%" alt=""></div><div class="item-work-describe"><p class="item-work-name">'+ json[index].title +'</p><!--<p class="item-work-msg">'+ json[index].describe +'</p>--></div></li>');
        $(".list-work").append('<li class="item-work" data-num='+ index +'><div class="item-work-imgbox"><div class="item-work-img"><div class="item-work-img-content" style="background-image: url(' + works[index].image +');"></div></div></div><div class="item-work-describe"><p class="item-work-name">'+ json[index].title +'</p><!--<p class="item-work-msg">'+ json[index].describe +'</p>--></div></li>');
      });
    });
  };
  function getClients(){
    $.getJSON('/javascripts/clients.json', function(json, textStatus) {
      $.each(json, function(index,mydata) {
        clients[index] = {};
        clients[index] = json[index];
        $(".list-client").append('<li class="item-client" data-num='+ index +' ><img class="item-client-img" src='+ clients[index].logo +' alt=""><div class="item-client-describe"><p class="item-client-name">'+ clients[index].name +'</p><p class="item-client-msg">'+ clients[index].describe +'</p></div></li>');
      });
    });
  };
  //click event to toggle lightboz
  $("body").on("click",".do-openpopup",function(){
      $(".layout-lightbox").addClass("is-open").delay(100).queue(
        function(next){$(this).addClass("is-showing");
      next();});
  });
  $("body").on("click",".do-closepopup",function(){
      $(".layout-lightbox").removeClass("is-showing").delay(900).queue(
        function(next){$(this).removeClass("is-open");
      next();});
  });
  //click events to switch menu
  $("#header").on("click",".icon-burger,.header-submenu",function(){
      $("#header").toggleClass("is-open");
  });

  $(".content-section-title").hover(function(){
      $(this).removeClass("is-playing").delay(50).queue(
        function(next){ $(this).addClass("is-playing");
      next();});
  },function(){
      // $(this).addClass("is-playing");
  });
  $("body").on("click",".goto-about",function(){
      $(".section-workdetail").removeClass("is-showing");
      $("body").find(".content-section-title").removeClass("is-playing");
      $("body").removeClass().addClass("is-open-about");
      var nextscroll = $('.section-boxbtn').offset().top;
      $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
      window.location = "#about";
  });
  $("body").on("click",".goto-portofolio",function(){
      $(".section-workdetail").removeClass("is-showing");
      $("body").find(".content-section-title").removeClass("is-playing");
      $("body").removeClass().addClass("is-open-portofolio");
      var nextscroll = $('.section-boxbtn').offset().top;
      $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
      window.location = "#portofolio";
  });
  $("body").on("click",".goto-clients",function(){
      $(".section-workdetail").removeClass("is-showing");
      $("body").find(".content-section-title").removeClass("is-playing");
      $("body").removeClass().addClass("is-open-clients");
      var nextscroll = $('.section-boxbtn').offset().top;
      $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
      window.location = "#clients";
  });
  $("body").on("click",".goto-contact",function(){
      $(".section-workdetail").removeClass("is-showing");
      $("body").find(".content-section-title").removeClass("is-playing");
      $("body").removeClass().addClass("is-open-contact");
      var nextscroll = $('.section-boxbtn').offset().top;
      $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 800);
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
      window.location = "#contact";
  });
  $("body").on("click",".goto-home",function(){
      $(".section-workdetail").removeClass("is-showing");
      $("body").find(".content-section-title").removeClass("is-playing");
      $("body").removeClass().addClass("is-loaded");
      $("#header").removeClass("is-open");
      window.location = "#";
      // $('html,body').animate({ scrollTop:0 }, 800);
  });

  //click events to switch focus style
  $("body").on("focus",".module-form-input,.module-form-textarea",function(){
    $("body").find(".is-focus").removeClass('is-focus');
    $(this).closest(".form-block").addClass("is-focus");
  });
  $("body").on("blur",".module-form-input,.module-form-textarea",function(){
    $(this).closest(".form-block").removeClass("is-focus");
  });

  function openDetail(myindex){
    $(".section-workdetail").removeClass("is-showing").queue(
    function(next){
      $(this).find(".workdetail-title-name").html( works[myindex].title + '<a class="workdetail-title-url" href='+ works[myindex].url +' target="_blank" ><i class="module-icon icon-link"></i></a>');
      $(this).find(".workdetail-msg").text(works[myindex].describe);
      //detail img may be 1~4 pieces
      $(this).find(".workdetail-img-list").html('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage1 +' alt=""></li>');
      if( works[myindex].detailimage2 != '' ){
        $(this).find(".workdetail-img-list").append('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage2 +' alt=""></li>');
      }
      if( works[myindex].detailimage3 != '' ){
        $(this).find(".workdetail-img-list").append('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage3 +' alt=""></li>');
      }
      if( works[myindex].detailimage4 != '' ){
        $(this).find(".workdetail-img-list").append('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage4 +' alt=""></li>');
      }
      //relative works must be 3 pieces
      // $(this).find(".workdetail-relative-list").html('<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate1 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate1].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate2 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate2].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate3 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate3].image+' width="100%" alt=""></div></li>');
      $(this).find(".workdetail-relative-list").html('<li class="workdetail-relative-item item-work" data-num=' + works[myindex].relate1 + '><div class="workdetail-relative-item-img"><div class="workdetail-relative-item-img-content" style="background-image: url(' + works[works[myindex].relate1].image + ');"></div></div></li>'+'<li class="workdetail-relative-item item-work" data-num=' + works[myindex].relate2 + '><div class="workdetail-relative-item-img"><div class="workdetail-relative-item-img-content" style="background-image: url(' + works[works[myindex].relate2].image + ');"></div></div></li>'+'<li class="workdetail-relative-item item-work" data-num=' + works[myindex].relate3 + '><div class="workdetail-relative-item-img"><div class="workdetail-relative-item-img-content" style="background-image: url(' + works[works[myindex].relate3].image + ');"></div></div></li>');
    next();}).delay(300).queue(
      function(next){$(this).addClass("is-showing");
    next();}); 
  };

  function switchDetail(myindex){
        $(".section-workdetail").addClass("is-fading").delay(350).queue(
        function(next){
          $(this).find(".workdetail-title-name").html(works[myindex].title + '<a class="workdetail-title-url" href='+ works[myindex].url +' target="_blank" ><i class="module-icon icon-link"></i></a>');
          $(this).find(".workdetail-msg").text(works[myindex].describe);
          $(this).find(".workdetail-img-list").html('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage1 +' width="100%" alt=""></li><li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage2 +' width="100%" alt=""></li><li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage3 +' width="100%" alt=""></li><li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage4 +' width="100%" alt=""></li>');
          //detail img may be 1~4 pieces
          $(this).find(".workdetail-img-list").html('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage1 +' width="100%" alt=""></li>');
          if( works[myindex].detailimage2 != '' ){
            $(this).find(".workdetail-img-list").append('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage2 +' width="100%" alt=""></li>');
          }
          if( works[myindex].detailimage3 != '' ){
            $(this).find(".workdetail-img-list").append('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage3 +' width="100%" alt=""></li>');
          }
          if( works[myindex].detailimage4 != '' ){
            $(this).find(".workdetail-img-list").append('<li class="workdetail-img-item"><img class="item-workdetail-img" src='+ works[myindex].detailimage4 +' width="100%" alt=""></li>');
          }
          //relative works must be 3 pieces
          // $(this).find(".workdetail-relative-list").html('<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate1 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate1].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate2 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate2].image+' width="100%" alt=""></div></li>'+'<li class="workdetail-relative-item item-work" data-num='+ works[myindex].relate3 +'><div class="item-work-imgbox"><img class="workdetail-relative-item-img" src='+works[works[myindex].relate3].image+' width="100%" alt=""></div></li>');
          $(this).find(".workdetail-relative-list").html('<li class="workdetail-relative-item item-work" data-num=' + works[myindex].relate1 + '><div class="workdetail-relative-item-img"><div class="workdetail-relative-item-img-content" style="background-image: url(' + works[works[myindex].relate1].image + ');"></div></div></li>'+'<li class="workdetail-relative-item item-work" data-num=' + works[myindex].relate2 + '><div class="workdetail-relative-item-img"><div class="workdetail-relative-item-img-content" style="background-image: url(' + works[works[myindex].relate2].image + ');"></div></div></li>'+'<li class="workdetail-relative-item item-work" data-num=' + works[myindex].relate3 + '><div class="workdetail-relative-item-img"><div class="workdetail-relative-item-img-content" style="background-image: url(' + works[works[myindex].relate3].image + ');"></div></div></li>');
        next();}).delay(350).queue(
          function(next){$(this).removeClass("is-fading");
        next();});  
   
  };

  //click events to switch works detail
  $("body").on("click",".item-work",function(){
    var index = $(this).attr("data-num");
    window.location = "#work="+index ;
    if ( !$("body").find(".section-workdetail").hasClass("is-showing") ){    
      openDetail(index);
    }else{
      switchDetail(index);
    }
    var nextscroll = $('.section-workdetail').offset().top;
    $('html,body').animate({ scrollTop:( parseInt(nextscroll-100) )}, 600);
  });

  $(".section-boxbtn").on("click",".boxbtn",function (){
    $(this).closest('.section-boxbtn').toggleClass('is-active').find(".boxbtn").not(this).removeClass('is-on');
    $(this).toggleClass('is-on');
  });

  //get hash
  function getHash(){
    var myhash = location.hash.slice(1);
    var hasharray = myhash.split("=");
    // alert("hash="+hasharray[0]+" , hasharray[1]="+hasharray[1]);
    if ( hasharray[0] == "work" ){
      $(".layout-loading .form-btn").hide();
      removeLoading(1000);
      //goto portofolio
      var hashindex = parseInt(hasharray[1]);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-portofolio").delay(1000).queue(
          function(next){
            openDetail(hashindex);
            next();});
    } else
    if( hasharray[0] == "about" ){
      $(".layout-loading .form-btn").hide();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-about");
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
    } else
    if( hasharray[0] == "portofolio" ){
      $(".layout-loading .form-btn").hide();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-portofolio");
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
    } else
    if( hasharray[0] == "clients" ){
      $(".layout-loading .form-btn").hide();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-clients");
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
    } else
    if( hasharray[0] == "contact" ){
      $(".layout-loading .form-btn").hide();
      removeLoading(1000);
      $(".section-workdetail").removeClass("is-showing");
      $("body").removeClass().addClass("is-open-contact");
      $("body").find(".content-section-title").delay(1000).queue(
          function(next){
            $("body").find(".content-section-title").addClass("is-playing");
          next();}); 
    }
  }
  //auto add height
  $('textarea').on("focus",function(){
    var enterpressed = 0;
    $(this).on("keypress",function(e){
      var code = e.keyCode || e.which;
       if(code == 13) { //keycode of enter
         enterpressed ++;
         if(enterpressed > 2){
            var myheight = parseInt($(this).height());
            if ( myheight < 200 ){
              $(this).height( myheight+24);
            }
         }
       }
    });
  });

  //animation
  function AnimateLine1(){
    //for slogan line1
    $("body").find(".bg-main.bg-main1").find(".shape-line.line-1").remove();
    $("body").find(".bg-main.bg-main1").append('<i class="shape-line line-1"></i>')
    .find(".shape-line.line-1").css("margin-left", Math.random()*(-400) ).addClass("do-animate")
    .delay(7000).queue(function(next){
      AnimateLine1();
      next();});
    };
  function AnimateLine2(){
    //for slogan line2
    $("body").find(".bg-main.bg-main1").find(".shape-line.line-2").remove();
    $("body").find(".bg-main.bg-main1").append('<i class="shape-line line-2"></i>')
    .find(".shape-line.line-2").css("margin-left", Math.random()*(-300) ).addClass("do-animate")
    .delay(13000).queue(function(next){
      AnimateLine2();
      next();});
    };
  function AnimateLine3(){
    $("body").find(".layout-loading-top-bg.loading-top-bg1").find(".shape-line.line-1").remove();
    $("body").find(".layout-loading-top-bg.loading-top-bg1").append('<i class="shape-line line-1"></i>')
    .find(".shape-line.line-1").css("margin-left", (Math.random()-0.5)*200 ).addClass("do-animate")
    .delay(10000+(Math.random()-0.5)*1000).queue(function(next){
      AnimateLine3();
      next();});
    };
  function AnimateLine4(){
    $("body").find(".layout-loading-top-bg.loading-top-bg1").find(".shape-line.line-2").remove();
    $("body").find(".layout-loading-top-bg.loading-top-bg1").append('<i class="shape-line line-2"></i>')
    .find(".shape-line.line-2").css("margin-left", (Math.random()-0.5)*300 ).addClass("do-animate")
    .delay(13000+(Math.random()-0.5)*1000).queue(function(next){
      AnimateLine4();
      next();});
    };
  function AnimateLine5(){
    $("body").find(".layout-loading-top-bg.loading-top-bg1").find(".shape-line.line-3").remove();
    $("body").find(".layout-loading-top-bg.loading-top-bg1").append('<i class="shape-line line-3"></i>')
    .find(".shape-line.line-3").css("margin-left", (Math.random()-0.5)*250 ).addClass("do-animate")
    .delay(7000+(Math.random()-0.5)*1000).queue(function(next){
      AnimateLine5();
      next();});
    };

  function doSloganAnimate(){
    AnimateLine1();
    AnimateLine2();
  }
  function doLoadingAnimate(){
    AnimateLine3();
    AnimateLine4();
    AnimateLine5();
  }
  // 執行 FastClick
  FastClick.attach(document.body);
  getWorks();
  getClients();
  getHash();
  // doLoadingAnimate();
  // doSloganAnimate();
})();