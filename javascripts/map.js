//$("#map-canvas").height(sheight-208);

function initializemap() {
// Create an array of styles.
  var styles = [
    {
      stylers: [
        // { hue: "#000000" },
        // { color: "#dddddd" },
        { saturation: -100 },
        { lightness: 20 }
      ]
    },{
      featureType: "road",
      elementType: "geometry",
      stylers: [
        { lightness: -20 }
      ]
    },{
      featureType: "water",
      elementType: "geometry",

      stylers: [
        { color: "#FFFFFF" }
        // { saturation: 0 },
        // { lightness: 100 }
      ]
    },{
      featureType: "all",
      elementType: "labels",
      stylers: [
        { visibility: "simplified" }
      ]
    },{
      featureType: "road",
      elementType: "labels"
    },{
      elementType: "labels.text.fill",
      stylers: [
        { color: "#333333" }
      ]
    }
  ];
  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});

  var myLatlng = new google.maps.LatLng(25.0712154,121.5129329);

  var mapOptions = {
    zoom: 17,
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    },
    center: {lat: 25.0712154, lng: 121.5129329},
    mapTypeControl: true,
    scrollwheel: false,
    zoomControl: true,
    scaleControl: true,
    streetViewControl:true
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  var markerimage = {
      url: 'images/icon-marker3.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(56, 75),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(28, 75)
    };

  var marker = new google.maps.Marker({
      position: myLatlng,
      icon: markerimage,
      map: map,
      title: '台北市大同區重慶北路三段252巷16號1樓'
  });
  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
  // map.setOptions({ scrollwheel: false });

}

google.maps.event.addDomListener(window, 'load', initializemap());